import { createStackNavigator } from "react-navigation";
import Home from "./pages/home";
import WebView from "./pages/webView";
import Product from "./pages/product";
import Cadastro from "./pages/cadastro";

export default createStackNavigator(
    {
        Home,
        WebView,
        Product,
        Cadastro
    },
    {
        navigationOptions: {
            headerStyle: {
                backgroundColor: "#4169E1"
            },
            headerTintColor: "#FFF"
        }
    }
);
