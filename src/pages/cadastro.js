import React, { Component } from "react";
import axios from "axios";
import api from "../services/api";

import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    StyleSheet,
    TextInput
} from "react-native";

export default class Cadastro extends Component {
    static navigationOptions = {
        title: "Sistema de Cadastro"
    };
    constructor() {
        super();

        this.state = {
            title: "",
            description: "",
            url: ""
        };
    }

    formSubmit = () => {
        api.post(`/products`, this.state);
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View>
                <TextInput
                    style={{
                        height: 40,
                        backgroundColor: "azure",
                        fontSize: 20,
                        borderWidth: 1,
                        borderColor: "#111",
                        width: 200,
                        margin: 20
                    }}
                    placeholder="Titulo"
                    onChangeText={title => this.setState({ title })}
                    value={this.state.title}
                />
                <TextInput
                    style={{
                        height: 40,
                        backgroundColor: "azure",
                        fontSize: 20,
                        borderWidth: 1,
                        borderColor: "#111",
                        width: 200,
                        margin: 20
                    }}
                    placeholder="Descrição"
                    onChangeText={description => this.setState({ description })}
                    value={this.state.description}
                />
                <TextInput
                    style={{
                        height: 40,
                        backgroundColor: "azure",
                        fontSize: 20,
                        borderWidth: 1,
                        borderColor: "#111",
                        width: 200,
                        margin: 20
                    }}
                    placeholder="url"
                    onChangeText={url => this.setState({ url })}
                    value={this.state.url}
                />

                <TouchableOpacity
                    style={style.productButton}
                    onPress={this.formSubmit.bind(this)}
                >
                    <Text>Criar</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const style = StyleSheet.create({
    productButton: {
        height: 42,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#4169E1",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    }
});
