import React, { Component } from "react";
import {
    Button,
    View,
    Text,
    FlatList,
    TouchableOpacity,
    StyleSheet
} from "react-native";

export default class Home extends Component {
    static navigationOptions = {
        title: "Lista de Projetos"
    };
    render() {
        return (
            <View style={style.container}>
                <TouchableOpacity
                    style={style.productButton}
                    onPress={() => {
                        this.props.navigation.navigate("WebView", {});
                    }}
                >
                    <Text style={style.productButtonText}>
                        Acessar a Web View
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={style.productButton}
                    onPress={() => {
                        this.props.navigation.navigate("Cadastro", {});
                    }}
                >
                    <Text style={style.productButtonText}>CRUD</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "transparent"
    },
    productButton: {
        height: 42,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#4169E1",
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    productButtonText: {
        fontSize: 16,
        color: "#4169E1",
        fontWeight: "bold"
    }
});
